
//#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,avx2,tune=native")
#pragma GCC optimization("unroll-loops")

#include <immintrin.h>
#include <bits/stdc++.h>
#include <unordered_map>
#include <unordered_set>
#include <chrono>
#include <random>


using namespace std;

typedef unsigned long long ull;
typedef long long ll;
typedef long double ld;
#define mp make_pair
#define pb push_back
#define rep(i, n) for (ll i = 0; i < (n); i++)
#define repb(i, n) for (ll i = n - 1; i>=0; --i)
#define all(x) x.begin(), x.end()
#define rall(x) x.rbegin(), x.rend()
const ll inf = 1e18;
mt19937_64 rng(chrono::steady_clock::now().time_since_epoch().count());
const int MAXN = 1e5;

struct Edge {
    int from = -1;
    int to = -1;
    ll cap = -1;
    ll f = -1;

    Edge(int _from, int _to, ll _cap, ll _f) : from(_from), to(_to), cap(_cap), f(_f) {};
};

int n, m;
vector<Edge> e;
vector<vector<int>> g;
vector<int> dist;
vector<int> ptr;

void add_edge(int from, int to, ll cap) {
    Edge eb = Edge(from, to, cap, 0);
    Edge er = Edge(to, from, 0, 0);
    g[from].pb(e.size());
    e.pb(eb);
    g[to].pb(e.size());
    e.pb(er);
}


bool bfs(int v, ll scale) {
    dist[v] = 0;
    queue<int> q;
    q.push(v);
    while (!q.empty() && dist[n - 1] == -1) {
        int v = q.front();
        q.pop();
        for (int id : g[v]) {
            int to = e[id].to;
            if (e[id].cap - e[id].f >= scale && dist[to] == -1) {
                dist[to] = dist[v] + 1;
                q.push(to);
            }
        }
    }
    return dist[n - 1] != -1;
}


void fill_again() {
    rep(i, n) {
        dist[i] = -1;
        ptr[i] = 0;
    }
}

ll dfs(int v, ll scale) {
    if (v == n - 1)
        return scale;
    for (; ptr[v] < g[v].size(); ++ptr[v]) {
        int id = g[v][ptr[v]];
        int to = e[id].to;
        if (dist[to] == dist[v] + 1 && e[id].cap - e[id].f >= scale) {
            ll res = dfs(to, scale);
            if (res > 0) {
                e[id].f += scale;
                e[id ^ 1].f -= scale;
                return scale;
            }
        }
    }
    return 0;
}


ll dinic() {
    ll flow = 0;
    dist.resize(n, -1);
    ptr.resize(n, 0);
    for (int pow = 32; pow >= 0; --pow) {
        fill_again();
        while (bfs(0, 1ll << pow)) {
            while (ll res = dfs(0, 1ll << pow))
                flow += res;
            fill_again();
        }
    }
    return flow;
}


inline void solve() {
    cin >> n >> m;
    g.resize(n);
    rep(i, m) {
        int a, b, c;
        cin >> a >> b >> c;
        a--, b--;
        add_edge(a, b, c);
    }
    cout << dinic();
}


inline void IQ() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
}

signed main() {
    // freopen("rollback.in", "r", stdin);
    // freopen("rollback.out", "w", stdout);
#ifdef GOSHA
    freopen("../IO/input.txt", "r", stdin);
    freopen("../IO/output.txt", "w", stdout);
#endif
    IQ();
    int T = 1;
    // cin >> T;
    while (T--) {
        solve();
    }
    return 0;
}