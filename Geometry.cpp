﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <stdio.h>
#include <queue>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <map>
#include <cassert>
#include <cmath>
#include <unordered_set>
#include <unordered_map>
#include <numeric>
#include <random>
#include <string>
using namespace std;
const int p = 31;
const int MAXN = 4 * 100000;
const int inf = 2e9;
#define modula 1000003
#define pi 3.14159265358979323846
#define endl "\n"
#define mp make_pair
#define pb push_back
#define pii pair<long long, long long>
#define pdd pair<double, double>
#define pll pair<long long, long long>
#define ll long long
#define ld long double
#define ull unsigned long long
#define veci vector<long long>
#define vecb vector <bool>
#define vecll vector <long long>
mt19937 rnd;
#define in "input.txt"
#define out "output.txt"

struct pt {
    ld x{ 0.0 }, y{ 0.0 };
    pt(ld x, ld y) {
        this->x = x;
        this->y = y;
    }

    pt operator+(pt& other) {
        return pt(x + other.x, y + other.y);
    }
    pt operator-(pt& other) {
        return pt(x - other.x, y - other.y);
    }
};

struct line {
    ld a, b, c;
    pt n = pt(a, b);
    pt p = pt(-b, a);
    pt onLine = pt(-a * c / (a * a + b * b), -b * c / (a * a + b * b));
    line(pt& p1, pt& p2) {
        this->a = p2.y - p1.y;
        this->b = p1.x - p2.x;
        this->c = -this->a * p1.x - this->b * p1.y;
        if (this->a == 0)
            this->a = 0.0;
        if (this->b == 0)
            this->b = 0.0;
        if (this->c == 0)
            this->c = 0.0;
        this->n = pt(this->a, this->b);
    }
    line(ld a, ld b, ld c) : a(a), b(b), c(c) {}
};


pt getPt(pt& p1, pt& p2) {
    return pt(p2.x - p1.x, p2.y - p1.y);
}
long double scalar(pt& p1, pt& p2) {
    return p1.x * p2.x + p1.y * p2.y;
}
long double product(pt& p1, pt& p2) {
    return p1.x * p2.y - p2.x * p1.y;
}

long double angleBetween(pt& p1, pt& p2) {
    return atan2(product(p1, p2), scalar(p1, p2));
}
long double triangleSquare(pt& p1, pt& p2, pt& p3) {
    pt p4 = getPt(p1, p2);
    pt p5 = getPt(p1, p3);
    return abs(product(p4, p5)) / 2.0;
}



pair<int, pt> lineIntersect(line& l1, line& l2) {
    if (l1.a * l2.b == l2.a * l1.b && l1.a * l2.c == l1.c * l2.a && l1.b * l2.c == l1.c * l2.b)
        return mp(2, pt(0, 0));
    else if (l1.a * l2.b == l1.b * l2.a)
        return mp(0, pt(0, 0));
    ld m = l1.a * l2.b - l1.b * l2.a;
    ld dx = l1.c * l2.b - l1.b * l2.c;
    ld dy = l1.a * l2.c - l1.c * l2.a;
    pair<int, pt> ans = mp(1, pt(-dx / m, -dy / m));
    if (ans.second.x == 0)
        ans.second.x = 0.0;
    if (ans.second.y == 0)
        ans.second.y = 0.0;
    return ans;
}
pt getThebaseOfPerpendicular(line l, pt p) {
	pt p1 = l.n + p;
	line l2 = line(p1, p);
	pair<int, pt> x = lineIntersect(l, l2);
	return x.second;
}
bool isPointInSegment(pt& p1, pt& p2, pt& p3) {
    pt v21 = getPt(p2, p1);
    pt v23 = getPt(p2, p3);
    pt v31 = getPt(p3, p1);
    pt v32 = getPt(p3, p2);
    if (abs(product(v23, v21)-0) <= 1e-9 && scalar(v21, v23)+1e-9 >= 0 && scalar(v31, v32)+1e-9 >= 0)
        return true;
    return false;
}

pair<bool, pt > segmentIntersect(pt p1, pt p2, pt p3, pt p4) {
    if (p1.x == p2.x && p1.y == p2.y) {
        if (isPointInSegment(p1, p3, p4))
            return mp(1, p1);
        return mp(0, p1);
    } else if (p3.x == p4.x && p3.y == p4.y) {
          if (isPointInSegment(p3, p1, p2))
              return mp(1, p3);
          return mp(0, p1);
      }
    line l1 = line(p1, p2);
    line l2 = line(p3, p4);
    pair<int, pt> ans = lineIntersect(l1, l2);
    if (ans.first == 0) {
        return mp(0, p1);
    } else if (ans.first == 1) {
          if (isPointInSegment(ans.second, p1, p2) && isPointInSegment(ans.second, p3, p4)) {
              return mp(1, ans.second);
          }
          return mp(0, p1);
      }
    if (isPointInSegment(p1, p3, p4))
        return mp(1, p1);
    else if (isPointInSegment(p2, p3, p4))
        return mp(1, p2);
    else if (isPointInSegment(p3, p1, p2))
        return mp(1, p3);
    else if (isPointInSegment(p4, p1, p2))
        return mp(1, p4);
    return mp(0, p1);
}

ld getSquareDistanceFromPointToLine(pt p, line l) {
    return ((l.a * p.x + l.b * p.y + l.c)*(l.a * p.x + l.b * p.y + l.c))/(l.a*l.a+l.b*l.b);
}

void normalizePoint(pt& p, ld d) {
    ld m = sqrt(p.x * p.x + p.y * p.y);
    p.x /= m;
    p.y /= m;
    p.x *= d;
    p.y *= d;
}

ld getSquareDistanceBetweenPoints(pt p1, pt p2) {
    return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
}
pt rotatePoint(pt p, pt c, ld sina, ld cosa) {
    pt ans = pt(0, 0);
    ans.x = (p.x - c.x) * cosa - (p.y - c.y) * sina + c.x;
    ans.y = (p.x - c.x) * sina + (p.y - c.y) * cosa + c.y;
    return ans;
}


inline void solve() {

}

inline void IQ() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
}

int main() {
#ifdef _DEBUG
    freopen(in, "r", stdin);
    freopen(out, "w", stdout);
#endif
    IQ();
    solve();
    return 0;
}
