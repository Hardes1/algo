﻿#define _CRT_SECURE_NO_WARNINGS
#include <bits/stdc++.h>
#include <chrono>
#include <random>
#include <unordered_map>
#include <unordered_set>
using namespace std;

typedef unsigned long long ull;
typedef long long ll;
typedef long double ld;
const unsigned p = 31;
const ll MAXN = 100000;
const ll INF = 2e9;
const long long INFl = 1e18;
const ll modula = 1e9 + 7;
const double pi = 3.14159265358979323846;
#define mp make_pair
#define pb push_back
#define forn(i, n) for (ll i = 0; i < n; i++)
#define in "movetofront.in"
#define out "movetofront.out"
#define pii pair<long long, long long>
#define pdd pair<double, double>
#define kv(x) x*x
#define pull pair<long long, long long>
#define veci vector<long long>
#define vecb vector <bool>
#define all(x) x.begin(), x.end()
typedef vector <unsigned long long> vecull;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());


struct pt {
	ll x;
	ll y;
	pt(ll x, ll y) {
		this->x = x;
		this->y = y;
	}
	pt() {
		x = 0;
		y = 0;
	}
	pt operator+(pt& other) {
		return pt(x + other.x, y + other.y);
	}
	pt operator-(pt& other) {
		return pt(x - other.x, y - other.y);
	}
	bool operator==(pt& other) {
		return x == other.x && y == other.y;
	}
	bool operator<(pt&other) {
		return x < other.x || (x == other.x && y < other.y);
	}
};
struct line {
	ld a, b, c;
	pt n = pt(a, b);
	pt p = pt(-b, a);
	pt onLine = pt(-a * c / (a * a + b * b), -b * c / (a * a + b * b));
	line(pt& p1, pt& p2) {
		this->a = p2.y - p1.y;
		this->b = p1.x - p2.x;
		this->c = -this->a * p1.x - this->b * p1.y;
		if (this->a == 0)
			this->a = 0.0;
		if (this->b == 0)
			this->b = 0.0;
		if (this->c == 0)
			this->c = 0.0;
		this->n = pt(this->a, this->b);
		n = pt(a, b);
		p = pt(-b, a);
		onLine = pt(-a * c / (a * a + b * b), -b * c / (a * a + b * b));
	}
	line(ld a, ld b, ld c) : a(a), b(b), c(c) {}
};


ll scalar(pt p1, pt p2) {
	return p1.x * p2.x + p1.y * p2.y;
}

ll product(pt p1, pt p2) {
	return p1.x * p2.y - p1.y * p2.x;
}

int getSDist(pt p1, pt p2) {
	return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
}
ld getDist(pt p1, pt p2) {
	return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

ld getDist(pt x, line l) {
	return abs(l.a * x.x + l.b * x.y + l.c) / (sqrt(l.a * l.a + l.b * l.b));
}

pair<int, pt> lineIntersect(line& l1, line& l2) {
	if (l1.a * l2.b == l2.a * l1.b && l1.a * l2.c == l1.c * l2.a && l1.b * l2.c == l1.c * l2.b)
		return mp(2, pt(0, 0));
	else if (l1.a * l2.b == l1.b * l2.a)
		return mp(0, pt(0, 0));
	ld m = l1.a * l2.b - l1.b * l2.a;
	ld dx = l1.c * l2.b - l1.b * l2.c;
	ld dy = l1.a * l2.c - l1.c * l2.a;
	pair<int, pt> ans = mp(1, pt(-dx / m, -dy / m));
	if (ans.second.x == 0)
		ans.second.x = 0.0;
	if (ans.second.y == 0)
		ans.second.y = 0.0;
	return ans;
}

pt getPt(pt& p1, pt& p2) {
	return pt(p2.x - p1.x, p2.y - p1.y);
}

struct node {
	node *l, *r;
	int val;
	int y;
	int cnt;
	int sum;
	node(int x) {
		val = x;
		y = rng();
		cnt = 1;
		sum = x;
		l = nullptr;
		r = nullptr;
	}
};


int sz(node * root) {
	if (root == nullptr)
		return 0;
	return root->cnt;
}

int getSum(node * root) {
	if (root == nullptr)
		return 0;
	return root->sum;
}


void upd(node * root) {
	if (root == nullptr)
		return;
	root->cnt = 1 + sz(root->l) + sz(root->r);
	root->sum = root->val + getSum(root->l) + getSum(root->r);
}


node * merge(node * root1, node * root2) {
	upd(root1);
	upd(root2);
	if (root1 == nullptr)
		return root2;
	if (root2 == nullptr)
		return root1;
	if (root1->y <= root2->y) {
		root1->r = merge(root1->r, root2);
		upd(root1);
		return root1;
	}
	else {
		root2->l = merge(root1, root2->l);
		upd(root2);
		return root2;
	}
}

pair<node *, node*> splitKth(node * root, int k) {
	upd(root);
	if (root == nullptr)
		return mp(nullptr, nullptr);
	int le = sz(root->l);
	if (k < le) {
		pair<node *, node *> cur = splitKth(root->l, k);
		root->l = cur.second;
		upd(root);
		return mp(cur.first, root);
	}
	else {
		pair<node*, node*> cur = splitKth(root->r, k - le - 1);
		root->r = cur.first;
		upd(root);
		return mp(root, cur.second);
	}
}

node * getKth(node * root, int k) {
	if (root == nullptr)
		return nullptr;
	int le = sz(root->l);
	if (k == le) {
		return root;
	}
	if (k < le) {
		return getKth(root->l, k);
	}
	else {
		return getKth(root->r, k - 1 - le);
	}
}

node * root = nullptr;
vector<node *> split(int l, int r) {
	l--, r--;
	pair<node *, node *> k = splitKth(root, r); 
	pair<node *, node *> x = splitKth(k.first, l - 1);
	return { x.first, x.second, k.second };
}
void merge1(node * r1, node *r2, node * r3) {
	root = merge(merge(r1, r2), r3);
}
inline void solve() {
}

inline void IQ() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
}

int main() {
	freopen(in, "r", stdin);
	freopen(out, "w", stdout);
#ifdef _DEBUG
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
#endif
	IQ();
	solve();
	return 0;
}